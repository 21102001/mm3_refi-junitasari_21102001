document.addEventListener("DOMContentLoaded", function () {
    const openAsideButton = document.getElementById("openAside");
    const aboutUsAside = document.querySelector(".aside");
  
    openAsideButton.addEventListener("click", function (e) {
      e.preventDefault();
      aboutUsAside.classList.toggle("open");
    });
  });
  
  